# **Siff::Result<T,E>**

 
This is a Rust-like result<T,E> third lib for error-handling
- T type object is available on success return
- E error object on failure

   

## inspiration

  

this project is inspired from
- [Rust's Result<T,E>](https://doc.rust-lang.org/std/result/)
>but is clearly not as developp, nor clean then the Rust One
 
- and from C++ third lib inpired form Rust too, [bitwizeshift/result](https://github.com/bitwizeshift/result)
>but clearly not as developp too


## features

  - header only
  -  single header
  - C++11 compatible

  
  >note : 
> **test** with visual 2015/2017/2019
> SiffResult<T,E> has less functionality then Rust'One or [bitwizeshift/result](https://github.com/bitwizeshift/result)
>but

>> just little piece of code, very understandable

>>No constraint of imposing a default constructor for T and E

>>usable as std::future's return

## snippet
using Siff::Result<T,E> in function
```cpp
Siff::Result<double, int> do_nothing(double * pVal){
    if(nullptr == pVal)
        return Siff::Fail(-1);
    return Siff::Resu(*pVal);
}
```
>the returns are now clearly identify as failure or success status.




consume fct in client
```cpp
double val {};
const auto resu_fct {do_nothing(&val)};    /// call fct here
```
> direct use of data
```cpp
if(resu_fct.has_value()){    /// tests if fct success
    const auto val {resu_fct.value_throw()};   /// consume the data
    std << cout << "the return value is " << val << std::endl;
}
```
>and/or error handling
```cpp
if(resu_fct.has_error()){    /// tests if fct failed
	/// error handling
	const auto error {resu_fct.error_throw()};	/// consume error
	std::cout << "error : " << error << std::endl;
	return;
}
```
> it's very important to test succes or failure first before trying
>>or it will **throw** an exception, that is clearly not the aim of this third lib...
>> but thanks to this behavior, there is No constraint of imposing a default constructor for T and E
