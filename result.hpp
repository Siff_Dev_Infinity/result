#pragma once
#include <memory>
#include <stdexcept>

namespace Siff
{
	template<typename T>
	struct struct_valid{
		T val;
	};

	template<typename T>
	struct_valid<T> Resu(T const& arVal){
		static_assert(std::is_copy_constructible<T>::value, "no copy constructor for T");
		return struct_valid<T>{arVal};
	}


	template<typename E>
	struct struct_error{
		E error;
	};

	template<typename E>
	struct_error<E> Fail(E const& arError){
		return struct_error<E>{arError};
	}


	template<typename T, typename E>
	class Result
	{
	public:
		Result()			= default;
		Result(Result const& arB){
			clone_private(arB);
		}

		Result& operator=(Result const& arB){
			clone_private(arB);
			return *this;
		}

		Result(Result&&)	= default;

		Result(struct_valid<T> const& arValue)
			: msp_value(std::make_shared<T>(arValue.val))
		{
		}

		Result(struct_error<E> const& arError)
			: msp_error(std::make_shared<E>(arError.error))
		{
		}

		T value_throw() const{
			if(nullptr == msp_value)
				throw std::logic_error("No value init");
			return *msp_value;
		}

		T or_default(T const& arDefault){
			if(has_value())
				return value_throw();
			return arDefault;
		}

		bool has_value() const noexcept{
			return msp_value != nullptr ? true : false;
		}

		E error_throw() const{
			if(nullptr == msp_error)
				throw std::logic_error("No error init");
			return *msp_error;
		}

		bool has_error() const noexcept{
			return msp_error != nullptr ? true : false;
		}

	private:
		void clone_private(Result const& arB){
			if(nullptr != arB.msp_value)
				msp_value = std::make_shared<T>(*arB.msp_value);
			if(nullptr != arB.msp_error)
				msp_error = std::make_shared<E>(*arB.msp_error);
		}
		std::shared_ptr<T> msp_value {};
		std::shared_ptr<E> msp_error {};
	};
}
